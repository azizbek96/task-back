<?php

/**
 * Created By PhpStorm
 * User Doston Usmonov
 * Time: 20.06.20 23:42
 */

namespace app\widgets\helpers;

use CURLFile;
use Exception;
use Yii;

class Telegram
{
	const JASURBEK = 749479441;

	public static $token = '1253118922:AAFRW-jXvHS_v7WfAbcE-5TztStWl_RS71Y'; //Samo Log bot

	public $text;
	public $id;
	public $module;
	public $controller;
	public $user;
	public $get_ip = false;

	public function __construct(
		$params = [
			'token' => '1253118922:AAFRW-jXvHS_v7WfAbcE-5TztStWl_RS71Y',
			'id' => 376544097,
			'text' => 'Salom',
			'module' => 'Tikuv',
			'controller' => 'Tikuv',
			'send_content' => false,
			'line' => __LINE__,
		]
	) {
		if ($_SERVER['SERVER_ADDR'] == '127.0.0.1' && !empty($params['id']) && !in_array($params['id'], ['64520993', '376544097'])) {
			return false;
		}
		self::$token = $params['token'] ?? '1253118922:AAFRW-jXvHS_v7WfAbcE-5TztStWl_RS71Y';
		$this->id = $params['id'] ?? 376544097;
		$this->user = \Yii::$app->user->identity->user_fio;
		$this->text = $params['text'] . "\n #line:" . $params['line'] ?? 'Text yozilmadi';
		$this->module = $params['module'] ?? 'Tikuv';
		$this->controller = $params['controller'] ?? 'TikuvGoodsDocPackController';
		if (empty($params['send_content'])) {
			return $this->sendMessage();
		}
	}

	public function sendMessage()
	{
		if ($this->get_ip) {
			$ip = $_SERVER['HTTP_CLIENT_IP'] ?? ($_SERVER['HTTP_X_FORWARDED_FOR'] ?? ($_SERVER['REMOTE_ADDR'] ?? '#not_ip'));
		} else {
			$ip = '';
		}
		define('TELEGRAM_TOKEN', self::$token);
		$url = $_SERVER['HTTP_HOST'];
		$ch = curl_init();
		$text =
			'#' .
			$url .
			' #' .
			$this->module .
			' ' .
			'#' .
			$this->controller .
			'\n ' .
			$this->text .
			'\n Ip : ' .
			$ip .
			'\n User #' .
			$this->user;
		curl_setopt_array($ch, [
			CURLOPT_URL => 'https://api.telegram.org/bot' . TELEGRAM_TOKEN . '/sendMessage',
			CURLOPT_POST => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_TIMEOUT => 10,
			CURLOPT_POSTFIELDS => [
				'chat_id' => $this->id,
				'parse_mode' => 'html',
				'text' => mb_substr($text, 0, 4096),
			],
		]);
		return curl_exec($ch);
	}
	public function sendContent()
	{
		file_get_contents(
			'https://api.telegram.org/bot' .
				self::$token .
				"/sendMessage?chat_id={$this->id}&text=" .
				urlencode($this->text) .
				'&parse_mode=Markdown',
		);
	}

	/**
	 * @param array $ids
	 * @param string $text
	 * @param array $localSendIds
	 * @param null $telegram_bot
	 * @return \Generator
	 */
	public static function sendMultiple(array $ids, string $text, array $localSendIds = [], $telegram_bot = null)
	{
		// 1053696039 -> Bahriddin
		$result = [];
		$host = $_SERVER['SERVER_ADDR'] ?? '';
		$serverName = $_SERVER['SERVER_NAME'];
		$url_server = ($_SERVER['SERVER_NAME'] ?? '') . "\n";
		$sended_text = $url_server . $text;
		$ip = $_SERVER['REMOTE_ADDR'] ?? ($_SERVER['HTTP_X_FORWARDED_FOR'] ?? '');
		if (!empty($ip)) {
			$sended_text .= "\n Ip: <code>" . $ip . '</code>';
		}
		$splited_text = !empty($sended_text) && is_string($sended_text) ? @mb_substr($sended_text, 0, 4095) : $sended_text;
		foreach ($ids as $id) {
			if (!in_array($id, $localSendIds) && ($host == '127.0.0.1' || $serverName == 'localhost')) {
				continue;
			}
			$options = [
				'chat_id' => $id,
				'parse_mode' => 'html',
				'text' => $splited_text,
			];
			$telegram_bot = is_null($telegram_bot) ? '1253118922:AAFRW-jXvHS_v7WfAbcE-5TztStWl_RS71Y' : $telegram_bot;
			$url = 'https://api.telegram.org/bot' . $telegram_bot . '/sendMessage';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $options);
			$res = curl_exec($ch);
			if (!empty(curl_error($ch))) {
				$result[$id] = json_encode(curl_error($ch));
			}
			$result[$id] = json_decode($res, 1);
		}
		if ($host !== '127.0.0.1') {
			$_GET['telegram_respons'] = $result;
		}
		return $result;
	}

	public static function getMessageSend($text, $module = null, $controller = null, $action = null, $line = null, $model = null)
	{
		$subText = json_encode($text, JSON_PRETTY_PRINT);
		$user = Yii::$app->user->identity->username;
		$url = $_SERVER['HTTP_HOST'];
		$sendText =
			'#<b>' .
			$url .
			"</b> \n controller: <b>" .
			$module .
			'/' .
			$controller .
			'/' .
			$action .
			"</b> \n\n " .
			$subText .
			"\n\n line: " .
			$line .
			" \n user: #" .
			$user;
		$splited_text = !empty($sendText) && is_string($sendText) ? @mb_substr($sendText, 0, 4095) : $sendText;
		$options = [
			'chat_id' => self::DOSTON,
			'parse_mode' => 'html',
			'text' => $splited_text,
		];
		$telegram_bot = '1119831722:AAFlkTz8jzSQn4g4b-AHs-CThDKrBmP9wF0';
		$url = 'https://api.telegram.org/bot' . $telegram_bot . '/sendMessage';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $options);
		curl_exec($ch);
	}

	public static function sendDocument($ids, $filePath, $caption = '', $local = [], $telegramToken = null)
	{
		$host = $_SERVER['SERVER_ADDR'] ?? '127.0.0.1';
		$caption = !empty($caption) && is_string($caption) ? @mb_substr($caption, 0, 1024) : $caption;
		$result = [];
		$file = new CURLFile($filePath);
		$token = $telegramToken ?? self::$token;
		foreach ($ids as $id) {
			if (!in_array($id, $local) && $host == '127.0.0.1') {
				continue;
			}
			$res = self::request(
				[
					'chat_id' => $id,
					'caption' => $caption,
					'parse_mode' => 'html',
					'document' => $file,
				],
				'sendDocument',
				$token,
			);
			$result[$id] = json_decode($res, 1);
		}
		$_GET['telegram_respons'][__LINE__] = $result;
		return $result;
	}
	public static function request($options, $method, $token = null)
	{
		$telegram_bot = $token ?? '1119831722:AAFlkTz8jzSQn4g4b-AHs-CThDKrBmP9wF0';
		$url = 'https://api.telegram.org/bot' . $telegram_bot . '/' . $method;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $options);
		$res = curl_exec($ch);
		if (!empty(curl_error($ch))) {
			return curl_error($ch);
		}
		return $res;
	}
	public static function sendPhoto($ids, $filePath, $caption = '', $local = [], $telegramToken = null)
	{
		$host = $_SERVER['SERVER_ADDR'];
		foreach ($ids as $id) {
			if (!in_array($id, $local) && $host == '127.0.0.1') {
				continue;
			}
			$res = self::request(
				[
					'chat_id' => $id,
					'caption' => $caption,
					'parse_mode' => 'html',
					'photo' => new \CURLFile($filePath),
				],
				'sendPhoto',
				$telegramToken,
			);
			$result[$id] = json_decode($res, 1);
		}
		$_GET['telegram_response'][__LINE__] = $result;
		return $result;
	}

	public static function tgExceptionLog(array $ids, $e, $send_local_ids = [], $text = '')
	{
		if ($e instanceof Exception == false) {
			return false;
		}
		return self::sendMultiple(
			$ids,
			json_encode(
				[
					'text' => $text,
					'line' => $e->getLine(),
					'message' => $e->getMessage(),
					'file' => $e->getFile(),
					'trace' => $e->getTrace(),
				],
				JSON_PRETTY_PRINT,
			),
			$ids,
		);
	}

	public static function sendArray(array $ids, $e, $send_local_ids = [])
	{
		if (is_array($e) == false) {
			return false;
		}
		return self::sendMultiple($ids, json_encode($e, JSON_PRETTY_PRINT), $send_local_ids);
	}

	public function sendVoice($chat_id, $voice, $caption)
	{
		if ($this->get_ip) {
			$ip = $_SERVER['HTTP_CLIENT_IP'] ?? ($_SERVER['HTTP_X_FORWARDED_FOR'] ?? ($_SERVER['REMOTE_ADDR'] ?? '#not_ip'));
		} else {
			$ip = '';
		}
		define('TELEGRAM_TOKEN', self::$token);
		$filepath = realpath($_FILES['file']['tmp_name']);
		$post_data = [
			'chat_id' => $chat_id,
			'voice' => new \CURLFile($filepath),
			'caption' => $caption,
		];
		$ch = curl_init();
		curl_setopt_array($ch, [
			CURLOPT_URL => 'https://api.telegram.org/bot' . TELEGRAM_TOKEN . '/sendVoice',
			CURLOPT_POST => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_TIMEOUT => 10,
			CURLOPT_POSTFIELDS => $post_data,
		]);
		return curl_exec($ch);
	}
}
