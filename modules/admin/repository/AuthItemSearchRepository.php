<?php

namespace app\modules\admin\repository;
use app\components\PermissionHelper as P;
use app\modules\admin\models\AuthItem;
use app\modules\admin\models\AuthItemSearch;
use Yii;
class AuthItemSearchRepository
{
    protected AuthItemSearch $authItemSearch;

    public function __construct(AuthItemSearch $authItemSearch)
    {
        $this->authItemSearch = $authItemSearch;
    }

    /**
     * @param array $params
     * @param int $page
     * @return array
     */
    public function indexRole(array $params=[], int $page=1){
        $searchModel = $this->authItemSearch;
        $dataProvider = $searchModel->search($params, $page);
        $totalCount = $dataProvider->totalCount;
        $pagination = $dataProvider->pagination;

        $role_type = [];

        foreach (AuthItem::getRoleType() as $key => $rt) {
            $role_type[] = [
                'label' => $rt,
                'value' => $key,
            ];
        }
        return [
            'dataProvider' => $dataProvider,
            'additionalData' => [
                'role_type' => $role_type,
                'pagination' => [
                    'totalSize' => $totalCount,
                    'page' => $pagination->page + 1,
                    'sizePerPage' => $pagination->pageSize,
                    'pageCount' => ceil($totalCount / $pagination->pageSize),
                ],
                'permissions' => [
                    'create' => P::can('auth-item/create-rule'),
                    'update' =>true,// P::can('auth-item/update-role'),
                    'delete' => P::can('auth-item/delete'),
                    'view' => P::can('auth-item/view'),
                ],
            ]
        ];
    }
}