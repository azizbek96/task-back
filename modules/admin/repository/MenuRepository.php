<?php

namespace app\modules\admin\repository;
use Yii;
use app\components\PermissionHelper;

class MenuRepository
{
    protected PermissionHelper $permissionHelper;
    public function __construct(PermissionHelper $permissionHelper)
    {
        $this->permissionHelper = $permissionHelper;
    }

    /**
     * @return array
     */
    public function menu(){
        $menu = [
            [
                'label' => 'Admin',
                'icon' => 'bx bx-cog',
                'visible' => $this->permissionHelper::orCan([
                    'user/index',
                    'auth-item/index-rule',
                    'auth-item/index-permission',
                    'employee/index',
                ]),
                'content' => [
                    [
                        'to' => '/users',
                        'label' => Yii::t('app', 'Users'),
                        'icon' => 'fas fa-users',
                        'visible' => $this->permissionHelper::can('users/index'),
                    ],
                    [
                        'to' => '/user-roles',
                        'label' => Yii::t('app', 'User Roles'),
                        'icon' => 'bx bx-key',
                        'visible' =>  $this->permissionHelper::can('auth-item/index-rule'),
                    ],
                    [
                        'to' => '/user-permissions',
                        'icon' => 'bx bx-dialpad-alt',
                        'label' => Yii::t('app', 'User Permissions'),
                        'visible' =>  $this->permissionHelper::can('auth-item/index-permission'),
                    ],
                    [
                        'label' => Yii::t('app', 'Employees'),
                        'to' => '/employees',
                        'icon' => 'far fa-address-card',
                        'visible' => $this->permissionHelper::can('employee/index'),
                    ],
                ],
            ],
            [
                'label' => Yii::t('app', 'File Uploaded'),
                'icon' => 'bx bx-wrench','visible' => $this->permissionHelper::orCan([
                    'employee-rel-attachment/index'
                ]),
                'content' => [
                    [
                        'label' => Yii::t('app', 'File'),
                        'to' => '/employee-rel-attachment',
                        'icon' => 'far fa-file',
                        'visible' => $this->permissionHelper::can('employee-rel-attachment/index'),
                    ],
                ]
            ],
        ];
        return $this->getCleaningMenu($menu);
    }

    /**
     * @param array $menu
     * @return array
     */
    protected function getCleaningMenu( array $menu): array
    {
        $newMenu = [];
        foreach ($menu as $menuKey => $menuItem) {
            if (!empty($menuItem['content']) ) {
                $newContent = [];
                foreach ($menuItem['content'] as $key => $item) {
                    if ($item['visible']) {
                        unset($menu[$menuKey]['content'][$key]['visible']);
                        $newContent[] = $menu[$menuKey]['content'][$key];
                    } else {
                        unset($menu[$menuKey]['content'][$key]);
                    }
                }
                $menuItem['content'] = $newContent;
                if (!empty($newContent))
                    $newMenu[] =$menuItem;
            }
        }
        return $newMenu;
    }
}