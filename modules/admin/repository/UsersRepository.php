<?php

namespace app\modules\admin\repository;

use app\modules\admin\models\AuthAssignment;
use app\modules\admin\models\Users;
use app\modules\api\models\BaseModel;
use Yii;

class UsersRepository
{
	public  function getList()
	{
		return Users::find()
			->select([
				'value' => 'id',
				'label' => 'username',
			])
			->where(['status' => BaseModel::STATUS_ACTIVE])
			->asArray()
			->all();
	}

	/**
	 * @param $data
	 * @param $id
	 * @return array|bool[]
	 */
	public static function saveModel($data, $id = null): array
	{
		$transaction = Yii::$app->db->beginTransaction();
		try {
			if ($id == null) {
				/**
				 * @var Users $model
				 * Yangi user yaratish
				 */
				$model = new Users();
				$model->scenario = Users::SCENARIO_CREATE;
				if ($data['password'] === $data['confirm_password']) {
					$model->setAttributes([
						'username' => $data['username'],
						'status' => BaseModel::STATUS_ACTIVE,
						'hr_employee_id' => $data['hr_employee_id'],
						'auth_key' => Yii::$app->security->generateRandomString(),
						'password_hash' => Yii::$app->security->generatePasswordHash($data['password']),
					]);
				} else {
					$model->addError('confirm_password', 'Password and Confirm Password not match');
					$transaction->rollBack();
					return [
						'status' => false,
						'errors' => $model->errors,
					];
				}
			} else {
				/**
				 * @var $model Users
				 * foydalanuvchi update qilish
				 */
				$model = Users::findOne($id);
				$model->scenario = Users::SCENARIO_UPDATE;
				$model->setAttributes([
					'username' => $data['username'],
					'status' => BaseModel::STATUS_ACTIVE,
					'hr_employee_id' => $data['hr_employee_id'],
				]);
			}

			if ($model->save()) {
				/**
				 * @var AuthAssignment $authAssignment
				 * Role biriktirish
				 */
				$roles = $data['roles'];
				if (!empty($roles)) {
					AuthAssignment::deleteAll(['user_id' => $model->id]);
					foreach ($roles as $key => $role) {
						$authAssignment = new AuthAssignment([
							'item_name' => $key,
							'user_id' => (string) $model->id,
							'created_at' => time(),
						]);
						if (!$authAssignment->save()) {
							$transaction->rollBack();
							return [
								'status' => false,
								'message' => Yii::t('app', 'Role saqlanmadi!'),
								'errors' => $authAssignment->getErrors(),
							];
						}
					}
				}
				$transaction->commit();
				return [
					'status' => true,
					'id' => $model->id,
				];
			} else {
				$transaction->rollBack();
				return [
					'status' => false,
					'errors' => $model->errors,
				];
			}
		} catch (\Exception $e) {
			$transaction->rollBack();
			return [
				'status' => false,
				'errors' => $e->getMessage(),
			];
		}
	}

    /**
     * @param $id
     * @return array
     */
	public function getUser($id): array
    {
		$models = Users::findOne(['id' => $id]);
		return [
			'id' => $models->id,
			'status' => $models->status,
			'username' => $models->username,
			'hr_employee_id' => $models->hrEmployee->first_name . ' ' . $models->hrEmployee->last_name . ' ' . $models->hrEmployee->father_name,
		];
	}

    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getUsernameAndHrEmployeeID($id){
        return Users::find()
            ->select(['id', 'hr_employee_id', 'username'])
            ->where(['id' =>$id])
            ->asArray()
            ->one();
    }

    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     */
    public function findModel($id)
    {
        return Users::find()
            ->where(['id' => $id])
            ->andWhere(['!=', 'status', BaseModel::STATUS_DELETED])
            ->one();
    }
}
