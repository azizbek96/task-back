<?php

namespace app\modules\admin\repository;

use app\modules\admin\models\AuthItemSearch;
use app\modules\api\responses\AuthItemResponse;
use Yii;
use app\modules\admin\models\AuthItem;
use app\modules\admin\models\BaseModel;
use app\components\PermissionHelper as P;
use app\modules\admin\models\AuthItemChild;
use yii\rbac\Item;

class AuthItemRepository
{
    protected AuthItemChild $authItemChild;
    protected AuthItem $authItem;
    protected BaseModel $baseModel;
    protected AuthAssignmentSearchRepository $authAssignmentSearchR;
    public function __construct( AuthItemChild $authItemChild,
                                 AuthAssignmentSearchRepository $authAssignmentSearchR,
                                 AuthItem $authItem,
                                 BaseModel $baseModel)
    {
        $this->authAssignmentSearchR = $authAssignmentSearchR;
        $this->authItemChild = $authItemChild;
        $this->baseModel = $baseModel;
        $this->authItem = $authItem;
    }


    /**
     * @param $params
     * @param $role
     * @return array|\yii\db\ActiveRecord[]
     */
    public function search($params, $role = false){

        $query = $this->authItem::find()
            ->select([
                'value' => 'name',
                'label' => '(CASE WHEN name_for_user ISNULL THEN name ELSE name_for_user END)',
            ])
            ->where([
                'type' => $role ? $this->baseModel::USER_ROLE_TYPE_ROLE : $this->baseModel::USER_ROLE_TYPE_PERMISSION,
            ]);

        $query->andWhere(['~*', 'name', $params['name']]);

        if (!empty($params['user_id'])) {
            $userRoles = array_map(function ($item) {
                return $item['value'];
            }, $this->authAssignmentSearchR->getUserRolesForSelect($params['user_id']));

            $query->andWhere(['NOT IN', 'name', $userRoles]);
        }

        return $query->asArray()->all();
    }

    /**
     * @param $key
     * @return array|mixed|string
     */
    public function getRoleType($key = null)
    {
        $data = [
            $this->baseModel::VIEW => Yii::t('app', 'View'),
            $this->baseModel::CREATE => Yii::t('app', 'Create'),
            $this->baseModel::DELETE => Yii::t('app', 'Delete'),
            $this->baseModel::UPDATE => Yii::t('app', 'Update'),
        ];
        if ($key) {
            return $data[$key] ?? '';
        }
        return $data;
    }
    public function getRolesForSelect(array $not = [])
    {
        $model = $this->authItem::find()
            ->select([
                'value' => 'name',
                'label' => '(CASE WHEN name_for_user ISNULL THEN name ELSE name_for_user END)',
            ])
            ->where(['type' => Item::TYPE_ROLE])
            ->orderBy(['created_at' => SORT_DESC, 'name' => SORT_ASC])
            ->limit(20)
            ->asArray();

        if (count($not) !== 0) {
            $not = array_map(function ($item) {
                return $item['value'];
            }, $not);

            $model->andWhere(['NOT IN', 'name', $not]);
        }

        return $model->all();
    }

    /**
     * @param $name
     * @return array|\yii\db\ActiveRecord|null
     */
    public function findModel($name){
        return $this->authItem::find()
            ->select(['name', 'data', 'category', 'role_type', 'rule_name', 'description', 'name_for_user'])
            ->where(['name' => $name])
            ->one();
    }

    /**
     * @param $data
     * @return void
     */
    public function createRole($data){
        return $this->authItem->setAttributes([
            'type' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            'name' => $data['name'],
            'role_type' => 1,
            'name_for_user' => $data['name_for_user'],
            'description' => $data['description'] ?? null,
        ]);
    }

    /**
     * @param $params
     * @return array
     */
    public function getSearch($params = []){
        $searchModel = new AuthItemSearch();
        $dataProvider = $searchModel->permission()->search($params);
        $totalCount = $dataProvider->totalCount;
        $pagination = $dataProvider->pagination;
        return [
            'dataProvider' => $dataProvider,
            "additional" => [
                'pagination' => [
                    'totalSize' => $totalCount,
                    'page' => $pagination->page + 1,
                    'sizePerPage' => $pagination->pageSize,
                    'pageCount' => ceil($totalCount / $pagination->pageSize),
                ],
                'permissions' => [
                    'create' => P::can('auth-item/create-permission'),
                    'view' => P::can('auth-item/view'),
                    'delete' => P::can('auth-item/delete'),
                    'update' => P::can('auth-item/update-permission'),
                ],
            ]
        ];
    }

    public function updateRole(AuthItem $model){
        $data = Yii::$app->request->post();
        $model->setAttributes([
            'updated_at' => time(),
            'name' => $data['name'],
            'type' => BaseModel::USER_ROLE_TYPE_ROLE,
            'name_for_user' => $data['name_for_user'],
        ]);
        if ($model->save()) {
            return $this->baseModel->success(AuthItemResponse::update($model, $data['old_name']));
        } else {
            return $this->baseModel->error(['errors' => $model->errors]);
        }
    }

    /**
     * @param AuthItem $model
     * @return array|void
     * @throws \yii\base\Exception
     */
    public function createPermission(AuthItem $model){
        $data = Yii::$app->request->post();
        $model->name = $data['name'];
        $model->type = BaseModel::USER_ROLE_TYPE_RULE;
        if ($model->save() && !empty($data['permissions'])) {
            $permissions = [];
            foreach ($data['permissions'] as $item) {
                $dataAI = [];
                $m = new AuthItem();
                $dataAI['AuthItem']['type'] = BaseModel::USER_ROLE_TYPE_PERMISSION;
                $dataAI['AuthItem']['category'] = $data['category'];
                $dataAI['AuthItem']['description'] = $item['description'];
                $dataAI['AuthItem']['name_for_user'] = $item['name_for_user'] ?? null;
                $dataAI['AuthItem']['name'] = $data['name'] . '/' . $item['name'];

                if ($m->load($dataAI) && $m->save()) {
                    $auth = Yii::$app->authManager;
                    $role = $auth->getRole($m->category);
                    $permission = $auth->getPermission($m->name);
                    $auth->addChild($role, $permission);
                    $permissions[] = AuthItemResponse::create($m);
                } else {
                    return $this->baseModel->error(['errors' => $m->errors]);
                }
            }
            return $this->baseModel->success($permissions);
        }
    }
}