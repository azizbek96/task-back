<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\AuthAssignment;

/**
 * AuthAssignmentSearch represents the model behind the search form of `app\modules\admin\models\AuthAssignment`.
 */
class AuthAssignmentSearch extends AuthAssignment
{
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [[['item_name', 'user_id'], 'safe'], [['created_at'], 'integer']];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * @param $params
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = AuthAssignment::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'created_at' => $this->created_at,
		]);

		$query->andFilterWhere(['like', 'item_name', $this->item_name])->andFilterWhere(['like', 'user_id', $this->user_id]);

		return $dataProvider;
	}

	public static function getUserRolesForSelect($id): array
	{
		$query = parent::find()
			->alias('aa')
			->select([
				'value' => 'ai.name',
				'label' => 'ai.name_for_user',
			])
			->where(['aa.user_id' => $id])
			->leftJoin('auth_item ai', 'aa.item_name = ai.name')
			->orderBy(['ai.created_at' => SORT_ASC]);

		return $query->asArray()->all();
	}
}
