<?php

namespace app\modules\admin\models;

use JetBrains\PhpStorm\ArrayShape;
use Yii;
use yii\behaviors\TimestampBehavior;
use app\components\OurCustomBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class BaseModel extends ActiveRecord
{
	const STATUS_DELETED = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 2;
	const STATUS_SAVED = 3;

	const CREATE = 1;
	const UPDATE = 2;
	const VIEW = 3;
	const DELETE = 4;

    const USER_ROLE_TYPE_ROLE = 1;
    const USER_ROLE_TYPE_PERMISSION = 2;
    const USER_ROLE_TYPE_RULE = 3;

    const CODE_ERROR = 403;
    const CODE_SUCCESS = 200;

    /**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			[
				'class' => OurCustomBehavior::class,
				'updatedByAttribute' => 'updated_by',
			],
			[
				'class' => TimestampBehavior::class,
			],
		];
	}

	/**
	 * @param $key
	 * @return array|mixed
	 */
	public static function getStatusList($key = null)
	{
		$result = [
			self::STATUS_DELETED => Yii::t('app', 'Deleted'),
			self::STATUS_ACTIVE => Yii::t('app', 'Active'),
			self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
			self::STATUS_SAVED => Yii::t('app', 'Saved'),
		];
		if (!is_null($key)) {
			return $result[$key];
		}
		return $result;
	}
    public function success($data = [], $other = [], string $message = ''): array
    {
        return ArrayHelper::merge($other, [
            'code' => BaseModel::CODE_SUCCESS,
            'message' => $message ?? Yii::t('app', 'Success'),
            'data' => $data,
        ]);
    }

    /**
     * @param array $data
     * @param string $message
     * @param int $code
     * @return array
     */
    #[ArrayShape(['data' => "array", 'code' => "mixed", 'message' => "string"])]
    public function error(array $data = [], string $message = '', int $code = BaseModel::CODE_ERROR): array
    {
        return [
            'data' => $data,
            'code' => $code,
            'message' => $message ?? Yii::t('app', 'Error'),
        ];
    }
}

new BaseModel();
