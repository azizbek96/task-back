<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\rbac\Item;

/**
 * This is the model class for table "auth_item".
 *
 * @property string $name
 * @property string $name_for_user
 * @property int $type
 * @property string $description
 * @property string $rule_name
 * @property resource $data
 * @property int $created_at
 * @property int $role_type
 * @property int $updated_at
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthRule $ruleName
 * @property AuthItemChild[] $authItemChildren
 * @property AuthItemChild[] $authItemChildren0
 * @property AuthItem[] $children
 * @property AuthItem[] $parents
 * @property string $category [varchar(64)]
 */
class AuthItem extends ActiveRecord
{
	/**
	 * @var array
	 */
	public $perms = [];

	/**
	 * @var array
	 */
	public $parents = [];

	/**
	 * @var array
	 */
	public $new_permissions = [];

	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'auth_item';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			['name', 'unique'],
			[['name', 'type'], 'required'],
			[
				'category',
				'required',
				'when' => function ($model) {
					return $model->type == 2;
				},
			],

			[
				'name',
				function ($attribute) {
					if ($this->type == 2) {
						if (!strpos($this->$attribute, '/')) {
							$this->addError($attribute, Yii::t('app', 'In name must be symbol "/" '));
						}
					}
					if ($this->type == 1) {
						if (strpos($this->$attribute, '/')) {
							$this->addError($attribute, Yii::t('app', 'In name cannot be symbol "/" '));
						}
					}
				},
			],
			[['type', 'created_at', 'updated_at', 'role_type'], 'integer'],
			[['description', 'data', 'name_for_user'], 'string'],
			[['name', 'rule_name'], 'string', 'max' => 64],
			[['name'], 'unique'],
			[['rule_name'], 'exist', 'skipOnError' => true, 'targetClass' => AuthRule::class, 'targetAttribute' => ['rule_name' => 'name']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'data' => Yii::t('app', 'Sana'),
			'type' => Yii::t('app', 'Type'),
			'name' => Yii::t('app', 'Roll nomi'),
			'category' => Yii::t('app', 'Category'),
			'rule_name' => Yii::t('app', 'Rule Name'),
			'role_type' => Yii::t('app', 'Role type'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'description' => Yii::t('app', 'Description'),
			'name_for_user' => Yii::t('app', 'Foydalanuvchi uchun roll nomi'),
		];
	}

	/**
	 * @param $insert
	 * @return bool
	 */
	public function beforeSave($insert)
	{
		if ($this->isNewRecord) {
			$this->rule_name = null;
			$this->data = null;
			$this->created_at = $time = time();
			$this->updated_at = $time;
		} else {
			$this->updated_at = time();
		}
		return parent::beforeSave($insert);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getAuthAssignments()
	{
		return $this->hasMany(AuthAssignment::class, ['item_name' => 'name']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getRuleName()
	{
		return $this->hasOne(AuthRule::class, ['name' => 'rule_name']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getAuthItemChildren()
	{
		return $this->hasMany(AuthItemChild::class, ['parent' => 'name']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getAuthItemChildren0()
	{
		return $this->hasMany(AuthItemChild::class, ['child' => 'name']);
	}

	/**
	 * @return ActiveQuery
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getChildren()
	{
		return $this->hasMany(AuthItem::class, ['name' => 'child'])->viaTable('auth_item_child', ['parent' => 'name']);
	}

	/**
	 * @return ActiveQuery
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getParents()
	{
		return $this->hasMany(AuthItem::class, ['name' => 'parent'])->viaTable('auth_item_child', ['child' => 'name']);
	}

	/**
	 * @param $child
	 * @return false|int|string|null
	 */
	public function checkPermissionChecked($child)
	{
		return AuthItemChild::find()
			->where(['parent' => $this->name])
			->andWhere(['child' => $child])
			->scalar();
	}

	/**
	 * @param null $name
	 * @return array
	 */
	public function getCategory($select = false, $name = null)
	{
		$models = AuthItem::find()
			->where(['type' => 1])
			->orderBy(['name' => SORT_ASC])
			->asArray();

		if (!is_null($name)) {
			$models->andWhere(['!=', 'name', $name]);
		}

		if ($select) {
			$models->addSelect([
				'value' => 'name',
				'label' => 'name_for_user',
			]);
		}

		return $select ? $models->all() : ArrayHelper::map($models->all(), 'name', 'name');
	}

	/**
	 * @param null $name
	 * @param bool $parent
	 * @return array
	 */
	public function getParentList($name = null, $parent = true)
	{
		if (!$parent) {
			$model = ArrayHelper::index(
				AuthItemChild::find()
					->where(['parent' => $name])
					->andWhere(['not like', 'child', '%/%', false])
					->asArray()
					->all(),
				'child'
			);
		} else {
			$model = ArrayHelper::index(
				AuthItemChild::find()
					->where(['child' => $name])
					->andWhere(['not like', 'child', '%/%', false])
					->asArray()
					->all(),
				'parent'
			);
		}
		return $model;
	}

	/**
	 * @param bool $isMap
	 * @return array
	 */
	public static function getRoles($isMap = false)
	{
		$models = AuthItem::find()
			->select(['name', 'name_for_user', 'role_type'])
			->where(['type' => Item::TYPE_ROLE])
			->orderBy([
				'role_type' => SORT_ASC,
				'name' => SORT_DESC,
			])
			->asArray()
			->all();
		if (!empty($models) && $isMap) {
			$models = ArrayHelper::index($models, 'name', 'role_type');
			if ($models) {
				foreach ($models as $key => $model) {
					$models[$key] = ArrayHelper::map($model, 'name', 'name_for_user');
				}
			}
		}
		return $models;
	}

    /**
     * @param array $not
     * @return array|ActiveRecord[]
     */
	public static function getRolesForSelect(array $not = [])
	{
		$model = AuthItem::find()
			->select([
				'value' => 'name',
				'label' => '(CASE WHEN name_for_user ISNULL THEN name ELSE name_for_user END)',
			])
			->where(['type' => Item::TYPE_ROLE])
			->orderBy(['created_at' => SORT_DESC, 'name' => SORT_ASC])
			->limit(20)
			->asArray();

		if (count($not) !== 0) {
			$not = array_map(function ($item) {
				return $item['value'];
			}, $not);

			$model->andWhere(['NOT IN', 'name', $not]);
		}

		return $model->all();
	}

	public static function getRoleWithPermission()
	{
		$roles = AuthItemChild::find()
			->alias('auc')
			->select(['auc.parent', 'auc.child', 'ai.name_for_user as role_name', 'ai2.name_for_user as per_name', 'ai2.role_type'])
			->leftJoin('auth_item ai', 'ai.name = auc.parent')
			->leftJoin('auth_item ai2', 'ai2.name = auc.child')
			->asArray()
			->all();
		return ArrayHelper::index($roles, null, 'parent');
	}

	/**
	 * @param $id
	 * @param bool $child
	 * @return array|ActiveRecord[]|null
	 */
	public static function getPermissionChild($id, bool $child = false)
	{
		if (!empty($id)) {
			return AuthItemChild::find()
				->where(['parent' => $id])
				->asArray()
				->all();
		}

		return null;
	}

	/**
	 * @param null $key
	 * @return array|mixed|string
	 */
	public static function getRoleType($key = null)
	{
		$data = [
			BaseModel::VIEW => Yii::t('app', 'View'),
			BaseModel::CREATE => Yii::t('app', 'Create'),
			BaseModel::DELETE => Yii::t('app', 'Delete'),
			BaseModel::UPDATE => Yii::t('app', 'Update'),
		];
		if ($key) {
			return $data[$key] ?? '';
		}
		return $data;
	}
}
