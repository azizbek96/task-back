<?php

namespace app\modules\api\controllers;

use Yii;
use yii\db\Exception;
use yii\db\ActiveRecord;
use app\controllers\BaseController;
use app\modules\api\models\BaseModel;
use app\modules\hrm\models\HrEmployees;
use app\components\PermissionHelper as P;
use app\modules\hrm\models\EmployeeRelAttachment;
use app\modules\hrm\models\search\EmployeeRelAttachmentSearch;
use app\modules\hrm\repository\EmployeeRelAttachmentRepository;

class EmployeeRelAttachmentController extends BaseController
{
    public EmployeeRelAttachmentRepository $employeeRelAttachmentRepository;
    public EmployeeRelAttachment $employeeRelAttachment;
    public function __construct($id, $module, $config = [],
                                EmployeeRelAttachmentRepository $employeeRelAttachmentRepository,
                                EmployeeRelAttachment $employeeRelAttachment )
    {
        $this->employeeRelAttachmentRepository = $employeeRelAttachmentRepository;
        $this->employeeRelAttachment = $employeeRelAttachment;
        parent::__construct($id, $module, $config);
    }

    /**
	 * @param int $page
	 * @return array
	 * @throws Exception
	 */
	public function actionIndex(int $page = 1): array
	{
		if ($this->request->isGet) {
			return $this->getEmployeeRelAttachmentPage($page);
		}
		return $this->error([], Yii::t('app', 'The query is incorrect'));
	}

	/**
	 * @param $id
	 * @return HrEmployees|array|null
	 */
	public function actionView($id)
	{
		if ($this->request->isGet) {
			$model = $this->findModel($id);
			if (!empty($model['code']) && $model['code'] == BaseModel::CODE_ERROR) {
				return $model;
			} else {
				return $this->success($model);
			}
		}
		return $this->error();
	}

	/**
	 * @return array
	 */
	public function actionCreate()
	{
		if ($this->request->isPost) {
           return $this->extracted(null);
		}
		return $this->error();
	}

    /**
     * @param $id
     * @return array
     */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if (empty($model)) {
			return $this->error([], Yii::t('app', 'Not Found'), 404);
		}

		if ($this->request->isPut) {
			return $this->extracted($id);
		}

		if ($this->request->isGet) {
			return $this->success($model->getUpdate($id));
		}
		return $this->error();
	}

	/**
	 * @param $id
	 * @return HrEmployees|array|null
	 */
	public function actionDelete($id)
	{
		if ($this->request->isDelete) {
			$model = $this->findModel($id);
            if (!empty($model['code']) && $model['code'] == BaseModel::CODE_ERROR) {
				return $model;
			} else {
                $response = $this->employeeRelAttachment->isDelete($model);
				return $response['status'] ? $this->getEmployeeRelAttachmentPage($page = 1, "Employee File successfuly deleted") : $this->error($response['errors']);
			}
		} else {
			return $this->error();
		}
	}
	/**
	 * @param $id
	 * @return array
	 */
	private function extracted($id = null): array
	{
		$data = $this->request->post();
		$response = $this->employeeRelAttachmentRepository::saveModel($data, $id);
		return $response['status'] ? $this->success($response['model'], [], Yii::t('app', 'Employee successfuly created')) : $this->error($response);
	}

    /**
     * @param $id
     * @return array|ActiveRecord
     */
	protected function findModel($id)
	{
		$model = $this->employeeRelAttachment->getFind($id);
		return $model !== null ? $model : $this->error();
	}

    /**
     * @param int $page
     * @param string $message
     * @return array
     */
	private function getEmployeeRelAttachmentPage(int $page = 1, string $message = ''): array
	{
        $searchModel = new EmployeeRelAttachmentSearch();
        $dataProvider = $searchModel->page($page);

		return $this->success($dataProvider['dataProvider'], [
			'pagination' => $dataProvider['pagination'],
			'permissions' => [
                'create' => P::can('employee-rel-attachment/create'),
                'view' => P::can('employee-rel-attachment/view'),
                'delete' => P::can('employee-rel-attachment/delete'),
                'update' => P::can('employee-rel-attachment/update'),
            ],
		], $message);
	}
}
