<?php

namespace app\modules\api\models;

use app\components\Constanta;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * @property string $id [integer]
 * @property string $size [integer]
 * @property string $name [varchar(255)]
 * @property string $path [varchar(255)]
 * @property string $extension [varchar(255)]
 * @property int $status [smallint]
 * @property int $type [smallint]
 * @property string $created_at [integer]
 * @property string $created_by [integer]
 * @property string $updated_by [integer]
 * @property string $updated_at [integer]
 */
class Attachments extends BaseModel
{
	public UploadedFile $file;

	private static string $basePath = 'hrm/employees';

	/**
	 * {@inheritdoc}
	 */
	public static function tableName(): string
	{
		return 'attachments';
	}

	public function rules(): array
	{
		$default = $this::getDefaultPath();

		return [
			[['path', 'extension'], 'string'],
			[['size', 'status', 'type'], 'integer'],
			['path', 'default', 'value' => $default],
			['name', 'default', 'value' => basename($default)],
			//            [
			//                'file',
			//                'file',
			//                'skipOnEmpty' => false,
			//                'mimeTypes' => 'image/*',
			//                'checkExtensionByMimeType' => true,
			//            ],
		];
	}

	private static function getDefaultPath()
	{
		return realpath(Attachments::pathJoin(dirname($_SERVER['SCRIPT_FILENAME']), 'uploads/defaults/user.png'));
	}

	private static function getPublicPath(): string
	{
		return Attachments::pathJoin(dirname($_SERVER['SCRIPT_FILENAME']), 'uploads');
	}

	/**
	 * @throws Exception
	 */
	public static function saveEmployeFile($file, $hrEmployeeId = null)
	{
		$saved = true;
		$path = Attachments::pathJoin(self::getPublicPath(), self::$basePath);
		if (!is_dir($path)) {
			FileHelper::createDirectory($path);
		}
		$transaction = Yii::$app->db->beginTransaction();
		try {
			$attachment = new Attachments();
			$name = Yii::$app->security->generateRandomString(16) . '-' . time();
			$attachment->file = $file;
			$attachment->name = $name;
			$attachment->size = $file->size;
			$attachment->path = Attachments::pathJoin('/uploads', self::$basePath, $name);
			$attachment->extension = pathinfo($file->name, PATHINFO_EXTENSION);

			if ($attachment->save()) {
				if (!$file->saveAs(Attachments::pathJoin($path, $name))) {
					$saved = false;
					$error = $file->getError();
				} else {
					$hera = HrEmployeesRelAttachments::findOne([
						'hr_employee_id' => $hrEmployeeId,
					]);
					if (!$hera) {
						$hera = new HrEmployeesRelAttachments();
					}
					$hera->setAttributes([
						'is_main' => 1,
						'hr_employee_id' => $hrEmployeeId,
						'attachment_id' => $attachment->id,
						'type' => Constanta::TYPE_IMAGE_AVATAR,
					]);
					if (!$hera->save()) {
						$saved = false;
						$error = $hera->getErrors();
					}
				}
			} else {
				$saved = false;
				$error = $attachment->getErrors();
			}
			if ($saved) {
				$transaction->commit();
				return [
					'status' => true,
				];
			} else {
				$transaction->rollBack();
				return [
					'status' => false,
					'errors' => $error ?? [],
				];
			}
		} catch (Exception $e) {
			$transaction->rollBack();
			return [
				'status' => false,
				'errors' => $e->getMessage(),
			];
		}
	}

	private static function pathJoin(...$parts)
	{
		if (sizeof($parts) === 0) {
			return '';
		}
		$prefix = $parts[0] === DIRECTORY_SEPARATOR ? DIRECTORY_SEPARATOR : '';
		$processed = array_filter(
			array_map(function ($part) {
				return rtrim($part, DIRECTORY_SEPARATOR);
			}, $parts),
			function ($part) {
				return !empty($part);
			}
		);
		return $prefix . implode(DIRECTORY_SEPARATOR, $processed);
	}
}
