<?php

namespace app\modules\hrm\controllers;

use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;

/**
 * Default controller for the `hrm` module
 */
class EmployeeController extends Controller
{
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator']['class'] = HttpBearerAuth::class;
		$behaviors['authenticator']['only'] = ['update'];
		return $behaviors;
	}
}
