<?php

namespace app\modules\hrm\repository;

use app\components\Constanta;
use app\modules\api\models\BaseModel;
use app\modules\hrm\models\EmployeeRelAttachment;
use app\modules\hrm\models\FileAttachment;
use app\modules\hrm\models\HrEmployees;
use Exception;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\UploadedFile;

class EmployeeRelAttachmentRepository extends EmployeeRelAttachment
{
	/**
	 * @return array
	 */
	public static function getList($isSelect = false): array
	{
		$list = self::find()
			->alias('era')
			->select([
                    'name' => new Expression("CONCAT(he.last_name,' ',he.first_name, ' ', he.father_name)"),
                ])
            ->leftJoin(['he' => 'hr_employees'],'era.hr_employee_id=he.id')
			->where(['e.status' => BaseModel::STATUS_ACTIVE]);
        $result = $list->asArray()->all();
		return $result;
	}

    /**
     * @return array
     */
	public static function getListMap(): array
	{
		return self::find()
			->select([
				'value' => 'id',
				'label' => "CONCAT(last_name, ' ', first_name, ' ', father_name)",
			])
			->where(['status' => BaseModel::STATUS_ACTIVE])
			->asArray()
			->all();
	}

	/**
	 * @param $data
	 * @param $id
	 * @return array
	 */
	public static function saveModel($data, $id)
	{
        $app = Yii::$app;
        $transaction = $app->db->beginTransaction();
        $response = [];
        $response['status'] = false;
        try {

            $userFiles = UploadedFile::getInstancesByName('user_files');
            if (!empty($userFiles)){
                foreach ($userFiles as $userFile){
                    $response = FileAttachment::uploadFile($userFile, 'user_files');
                    if ($response['status']) {
                        $model = new EmployeeRelAttachment();
                        $model->hr_employee_id = Yii::$app->user->identity->hr_employee_id;
                        $model->attachment_id = $response['result']['id'];
                        $model->status = BaseModel::STATUS_ACTIVE;
                        if ( $model->save() === false ) {
                            $response = [
                                'status' => false,
                                'message' => Yii::t('app', 'Model post data not saved'),
                                'errors' => $model->errors,
                            ];
                        }
                    }else{
                        break;
                    }
                }

            }else{
                $response = [
                    'status' => false,
                    'message' => Yii::t('app', 'Fayl yuklanmagan'),
                ];
            }
            if ($response['status']) {
                $response['message'] = Yii::t('app', 'Exam document saved successfully');
                $response['model'] = [];
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }

        } catch (Exception $e) {
            $transaction->rollBack();
            return [
                'status' => false,
                'errors' => $e->getMessage(),
            ];
        }
        return $response;
	}
}
