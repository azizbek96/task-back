<?php

namespace app\modules\hrm\models;

use Exception;
use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "file_attachment".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $native_path
 * @property string|null $absolute_path
 * @property string|null $hash_file
 * @property string|null $type
 * @property int|null $size
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 *
 * @property EmployeeRelAttachment[] $employeeRelAttachments
 */
class FileAttachment extends BaseModel
{
    const MAX_FILE_SIZE = 4194304; // 4 MB
    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'file_attachment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['size', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'default', 'value' => null],
            [['size', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['native_path', 'absolute_path','hash_file'], 'string', 'max' => 500],
            [['type'], 'string', 'max' => 100],
            [['file'], 'file', 'skipOnEmpty' => true,
                'checkExtensionByMimeType' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'native_path' => Yii::t('app', 'Native Path'),
            'absolute_path' => Yii::t('app', 'Absolute Path'),
            'hash_file' => Yii::t('app', 'Hash file'),
            'type' => Yii::t('app', 'Type'),
            'size' => Yii::t('app', 'Size'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[EmployeeRelAttachments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeRelAttachments()
    {
        return $this->hasMany(EmployeeRelAttachment::className(), ['attachment_id' => 'id']);
    }

    /**
     * @param $file
     * @param $path
     * @return array
     */
    public static function uploadFile($file, $path)
    {
        if (!is_string($path)) {
            return [
                'status' => false,
                'message' => Yii::t('app', "Path is not a string"),
            ];
        }
        $flag = true;
        $message = '';
        $errors = null;
        $result = null;

        try {
            $self = new static();
            $self->file = $file;
            $self->name = $file->name;
            $self->type = $file->type;
            $self->size = $file->size;
            $string = time();
            $webRoot = Yii::getAlias('@webroot');
            $self->native_path = '/uploads/' . $path . '/' . date('Y/m');
            if (!file_exists($webRoot . $self->native_path)) {
                FileHelper::createDirectory($webRoot . $self->native_path);
            }
            if (!file_exists($webRoot . '/uploads/copy')) {
                FileHelper::createDirectory($webRoot . '/uploads/copy');
            }
            $native_path =  '/uploads/copy/'. $string . $file->name;
            $self->native_path .= '/' . $string . $file->name;
            $self->absolute_path = $webRoot . $self->native_path;
            $absolute_path = $webRoot . $native_path;
            if (!$self->file->saveAs($absolute_path)) {
                $flag = false;
                $message = Yii::t('app', "File upload failed");
            }
            if (is_file($absolute_path))
                $hashFileModel = $self->hashFileAttachment($absolute_path);
            if (!empty($hashFileModel) && unlink($absolute_path)){
                $self->type = $hashFileModel['type'];
                $self->size = $hashFileModel['size'];
                $self->hash_file = $hashFileModel['hash_file'];
                $self->native_path = $hashFileModel['native_path'];
                $self->absolute_path = $hashFileModel['absolute_path'];
            }else{
                copy($absolute_path, $self->absolute_path);
                $self->hash_file = hash_file('md5', $self->absolute_path);
                unlink($absolute_path);
            }
            if ( $flag && !$self->save(false, ['name', 'native_path', 'absolute_path', 'type', 'size', 'hash_file'])) {
                $flag = false;
                $message = Yii::t('app', "File not saved");
                $errors = $self->errors;
            } else {
                $result = $self->attributes;
            }

        } catch (Exception $e) {
            $flag = false;
            $message = $e->getMessage();
            $errors = $e->getTrace();
            $result = $e->getTrace();
        }
        return [
            'status' => $flag,
            'message' => $message,
            'result' => $result,
            'filePath' => $self->absolute_path,
            'errors' => $errors
        ];
    }

    /**
     * @param string $absolute_path
     * @return array|\yii\db\ActiveRecord[]
     */
    protected function hashFileAttachment(string $absolute_path){
        return $this::find()->where(['hash_file' => hash_file('md5',$absolute_path)])->asArray()->one();
    }

    public static function getIsSearchFile(EmployeeRelAttachment $model){
        $result = FileAttachment::find()
            ->where(['hash_file' => FileAttachment::find()
                    ->select('hash_file')
                    ->where(['id' => 10])
                ])
            ->andWhere(['!=','id', 10])
            ->asArray()->all();
        if (empty($result)){
             return FileAttachment::find()->where(['id' => $model->attachment_id])
                ->andWhere(['status' => BaseModel::STATUS_ACTIVE])->one();
        }
        return [];
    }

}
