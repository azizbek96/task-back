<?php

namespace app\modules\hrm\models;

use app\modules\api\models\BaseModel;
use Yii;

/**
 * This is the model class for table "employee_rel_attachment".
 *
 * @property int $id
 * @property int|null $hr_employee_id
 * @property int|null $attachment_id
 * @property int|null $type
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 *
 * @property FileAttachment $attachment
 * @property HrEmployees $hrEmployee
 */
class EmployeeRelAttachment extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_rel_attachment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hr_employee_id', 'attachment_id', 'type', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'default', 'value' => null],
            [['hr_employee_id', 'attachment_id', 'type', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['attachment_id'], 'exist', 'skipOnError' => true, 'targetClass' => FileAttachment::className(), 'targetAttribute' => ['attachment_id' => 'id']],
            [['hr_employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => HrEmployees::className(), 'targetAttribute' => ['hr_employee_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'hr_employee_id' => Yii::t('app', 'Hr Employee ID'),
            'attachment_id' => Yii::t('app', 'Attachment ID'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[Attachment]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAttachment()
    {
        return $this->hasOne(FileAttachment::className(), ['id' => 'attachment_id']);
    }

    /**
     * Gets query for [[HrEmployee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHrEmployee()
    {
        return $this->hasOne(HrEmployees::className(), ['id' => 'hr_employee_id']);
    }

    /**
     * @return array|bool[]
     */
    public function isDelete(EmployeeRelAttachment $model): array
    {
        $model->status = BaseModel::STATUS_DELETED;
        $fileAttachment = FileAttachment::getIsSearchFile($model);
        if (empty($fileAttachment)){
            if ($model->save(false)){
                return ['status' => true];
            }else {
                return [
                    'status' => false,
                    'error' => $model->getErrors(),
                ];
            }
        }else {
            if($fileAttachment->delete() && $model->delete() && unlink($fileAttachment->absolute_path)){
                return ['status' => true];
            }else {
                return [
                    'status' => false,
                    'error' => $model->getErrors(),
                ];
            }
        }
    }

    public function getFind($id){
        return $this::find()
            ->where(['id' => (int) $id])
            ->andWhere(['!=', 'status', BaseModel::STATUS_DELETED])
            ->one();
    }
}
