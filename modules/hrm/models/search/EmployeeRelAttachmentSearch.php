<?php

namespace app\modules\hrm\models\search;

use app\modules\admin\models\AuthAssignment;
use app\modules\hrm\models\BaseModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\hrm\models\EmployeeRelAttachment;
use yii\data\SqlDataProvider;
use yii\db\Expression;

/**
 * EmployeeRelAttachmentSearch represents the model behind the search form of `app\modules\hrm\models\EmployeeRelAttachment`.
 */
class EmployeeRelAttachmentSearch extends EmployeeRelAttachment
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'hr_employee_id', 'attachment_id', 'type', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $params
     * @param int $page
     * @return array
     */
    public function search($params, int $page = 1): array
    {
        $query = EmployeeRelAttachment::find()->alias('era')
            ->select([
                "id" => 'era.id',
                "name" => new Expression("CONCAT(he.last_name,' ',he.first_name, ' ', he.father_name)"),
                "file_name" => 'fa.name',
                "size" => 'fa.size'
            ])
            ->leftJoin(['he' => 'hr_employees'],'era.hr_employee_id=he.id')
            ->leftJoin(['fa' => 'file_attachment'],'era.attachment_id=fa.id')
            ->where(['!=', 'era.status', BaseModel::STATUS_DELETED])
            ->groupBy(['era.id',"he.id","fa.id"]);
        // add conditions that should always apply here

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'hr_employee_id' => $this->hr_employee_id,
            'attachment_id' => $this->attachment_id,
            'type' => $this->type,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $command = $query->createCommand();

        $dataProvider = new SqlDataProvider([
            'sql' => $command->rawSql,
            'pagination' => [
                'pageSize' => 55,
                'page' => $page - 1,
            ],
            'sort' => [
                'attributes' => ['id', 'name', 'file_name', 'size'],
            ],
        ]);
        $totalCount = $dataProvider->getTotalCount();
        $pagination = $dataProvider->getPagination();
        return [
            'dataProvider' => $dataProvider->getModels(),
            'pagination' => [
                'totalSize' => $totalCount,
                'page' => $pagination->page + 1,
                'sizePerPage' => $pagination->pageSize,
                'pageCount' => ceil($totalCount / $pagination->pageSize),
            ],
        ];
    }

    /**
     * @param $params
     * @param int $page
     * @param null $departmentId
     * @return array
     */
    public function page(int $page = 1): array
    {
        $userIdList = AuthAssignment::getCurrentUser();
        $query = EmployeeRelAttachment::find()
            ->alias('era')
            ->select([
                "id" => 'era.id',
                "name" => new Expression("CONCAT(he.last_name,' ',he.first_name, ' ', he.father_name)"),
                "file_name" => 'fa.name',
                "file_path" => 'fa.absolute_path',
                "status" => 'era.status',
                "file_size" => 'fa.size'
            ])
            ->leftJoin(['he' => 'hr_employees'],'era.hr_employee_id=he.id')
            ->leftJoin(['fa' => 'file_attachment'],'era.attachment_id=fa.id')
            ->where(['!=','era.status', BaseModel::STATUS_DELETED]);
            if ($userIdList){
                $query ->andWhere(['era.created_by'=> $userIdList]);
            }
           $query->groupBy(['era.id',"he.id","fa.id"]);

        $command = $query->createCommand();

        $dataProvider = new SqlDataProvider([
            'sql' => $command->rawSql,
            'pagination' => [
                'pageSize' => 55,
                'page' => $page - 1,
            ],
            'sort' => [
                'attributes' => ['id', 'name', 'file_name', 'size'],
            ],
        ]);
        $totalCount = $dataProvider->getTotalCount();
        $pagination = $dataProvider->getPagination();
        return [
            'dataProvider' => $dataProvider->getModels(),
            'pagination' => [
                'totalSize' => $totalCount,
                'page' => $pagination->page + 1,
                'sizePerPage' => $pagination->pageSize,
                'pageCount' => ceil($totalCount / $pagination->pageSize),
            ],
        ];
    }


}