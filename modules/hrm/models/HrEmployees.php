<?php

namespace app\modules\hrm\models;

use app\modules\admin\models\Users;
use app\modules\api\models\BaseModel;
use app\modules\directory\models\DirectoryCountry;
use app\modules\directory\models\DirectoryDistricts;
use app\modules\directory\models\DirectoryMain;
use app\modules\directory\models\DirectoryRegions;
use app\modules\hrm\models\relation\HrDepartmentRelEmployee;
use app\modules\hrm\models\relation\HrEmployeeRelHabit;
use app\modules\hrm\models\relation\HrEmployeeRelOrganizations;
use app\modules\hrm\models\relation\HrEmployeeRelSkills;
use app\modules\hrm\models\relation\HrEmployeeRelStudyPlaces;
use app\modules\hrm\models\relation\HrEmployeesRelAttachments;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "hr_employees".
 *
 * @property int $id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $father_name
 * @property int|null $birth_date
 * @property string|null $address
 * @property string|null $phone_number
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by

 * @property-read array $create
 * @property-read string $fullName
 * @property Users[] $users
 */
class HrEmployees extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hr_employees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birth_date','phone_number'], 'safe'],
            [['address'], 'string'],
            [['last_name', 'father_name', 'first_name'], 'required'],
            [['first_name', 'last_name','father_name'],'string', 'max' => 255],
            [['phone_number'], 'string', 'max' => 20],
            [
                [
                    'status',
                    'type',
                    'created_at',
                    'created_by',
                    'updated_at',
                    'updated_by',
                ],
                'integer',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'father_name' => Yii::t('app', 'Father Name'),
            'birth_date' => Yii::t('app', 'birth_date'),
            'address' => Yii::t('app', 'Address'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'status' => Yii::t('app', 'Status'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @param $insert
     * @return bool
     */
    public function beforeSave($insert): bool
    {
        if (!empty($this->birth_date)) {
            $this->birth_date = strtotime($this->birth_date);

            if ($this->birth_date < 0) {
                $this->birth_date = null;
            }
        }
        if ($this->isNewRecord) {
            $this->status = BaseModel::STATUS_ACTIVE;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return void
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->birth_date = date('Y-m-d', $this->birth_date);
    }


    /**
     * Gets query for [[Users]].
     *
     * @return ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::class, ['hr_employee_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getCreate(): array
    {
        return [
            'id' => $this->id,
            'address' => $this->address,
            'last_name' => $this->last_name,
            'first_name' => $this->first_name,
            'father_name' => $this->father_name,
            'birth_date' => date('Y-m-d', $this->birth_date),
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function getUpdate($id)
    {
        $model = $this->getAttributes();
        unset($model['created_at'], $model['created_by'], $model['updated_at'], $model['updated_by'], $model['type']);
        return [
            'model' => $model,
        ];
    }

    /**
     * @return array|bool[]
     */
    public function isDelete(): array
    {
        $this->status = BaseModel::STATUS_DELETED;
        if ($this->save(false)) {
            return [
                'status' => true,
            ];
        } else {
            return [
                'status' => false,
                'error' => $this->getErrors(),
            ];
        }
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return ucfirst($this->last_name) . ' ' . ucfirst($this->first_name) . ' ' . ucfirst($this->father_name);
    }
}
