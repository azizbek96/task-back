<?php

namespace app\modules\hrm\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\components\OurCustomBehavior;
use yii\db\ActiveRecord;

class BaseModel extends ActiveRecord
{
	const STATUS_DELETED = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 2;
	const STATUS_SAVED = 3;

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			[
				'class' => OurCustomBehavior::class,
			],
			[
				'class' => TimestampBehavior::class,
			],
		];
	}

	/**
	 * @param $key
	 * @return array|mixed
	 */
	public static function getStatusList($key = null)
	{
		$result = [
			self::STATUS_ACTIVE => Yii::t('app', 'Active'),
			self::STATUS_SAVED => Yii::t('app', 'Saved'),
			self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
			self::STATUS_DELETED => Yii::t('app', 'Deleted'),
		];
		if (!is_null($key)) {
			return $result[$key];
		}

		return $result;
	}
}
