<?php

namespace app\components;

class Constanta
{
	/**
	 * @param int [hr_employees of type column]
	 */
	const TYPE_EMPLOYEE_REGISTRATION = 1; //ro'yhatga olingan hodimlar

	/** end  [hr_employees of type column] */

	//    ----------------

	/** @var int * [hr_employees_rel_attachments of type column] */

	const TYPE_IMAGE_AVATAR = 1; //main image

	/** end [hr_employees_rel_attachments] */

	/**
	 * @param int [hr_vacancy_rel_attachments of type column]
	 */
	const TYPE_DOCUMENT_SOP = 1; ///-	Asosiy Jarayonlar Instruksiyasi (SOP, fayl ko’rinishida)
	const TYPE_DOCUMENT_JES = 2; ///-	Qo’shimcha Jarayonlar Instruksiyasi (JES, Fayl ko’rinishida)

	/** end  [hr_employees_rel_attachments of type column] */


	const DOCUMENT_TYPE_HIRE_LABEL = 'hire';
	const DOCUMENT_TYPE_TRANSFER_LABEL = 'transfer';
	const DOCUMENT_TYPE_DISMISSAL_LABEL = 'dismissal';
	const DOCUMENT_TYPE_FINE_REWARD_LABEL = 'fine-reward';

	const DOCUMENT_TYPE_HIRE_SCENARIO = 'hire';
	const DOCUMENT_TYPE_TRANSFER_SCENARIO = 'transfer';
	const DOCUMENT_TYPE_DISMISSAL_SCENARIO = 'dismissal';
	const DOCUMENT_TYPE_FINE_REWARD_SCENARIO = 'fine-reward';

	/** end  [HrAppointmentDocument of document_type column] */
}
