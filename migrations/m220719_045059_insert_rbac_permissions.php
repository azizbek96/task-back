<?php

use yii\db\Migration;

/**
 * Class m220530_065941_insert_rbac_permissions
 */
class m220719_045059_insert_rbac_permissions extends Migration
{
    static $module = 'api';
    static $controller = 'employee-rel-attachment';
    static $actions = ['index', 'create', 'update', 'delete', 'view'];
    static $roleName = 'employee-rel-attachment';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->upsert('auth_item', ['name' => self::$roleName, 'type' => '1', 'description' => '','name_for_user' => 'Fayl yulash role']);
        foreach (self::$actions as $action) {
            $p = self::$controller . '/' . $action;
            $this->upsert('auth_item', ['name' => $p, 'type' => '2', 'description' => '']);
            $this->upsert('auth_item_child', ['parent' => self::$roleName, 'child' => $p]);
        }

        $this->upsert('auth_item_child', ['parent' => 'admin', 'child' => 'role-user']);
        $this->upsert('auth_item_child', ['parent' => 'admin', 'child' => 'role-auth-item']);
        $this->upsert('auth_item_child', ['parent' => 'admin', 'child' => 'employee-rel-attachment']);
        $this->upsert('auth_item_child', ['parent' => 'user', 'child' => 'employee-rel-attachment']);
        $this->upsert('auth_item_child', ['parent' => 'moderator', 'child' => 'employee-rel-attachment/index']);
        $this->upsert('auth_item_child', ['parent' => 'moderator', 'child' => 'employee-rel-attachment/update']);
        $this->upsert('auth_item_child', ['parent' => 'moderator', 'child' => 'employee-rel-attachment/delete']);
        $this->upsert('auth_item_child', ['parent' => 'moderator', 'child' => 'employee-rel-attachment/view']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        foreach (self::$actions as $action) {
            $p = self::$controller . '/' . $action;
            $this->delete('auth_item', ['name' => $p, 'type' => '2']);
            $this->delete('auth_item_child', ['parent' => self::$roleName, 'child' => $p]);
        }
        $this->delete('auth_item', ['name' => self::$roleName, 'type' => '1']);

        $this->delete('auth_item_child', ['parent' => 'admin', 'child' => 'role-user']);
        $this->delete('auth_item_child', ['parent' => 'admin', 'child' => 'role-auth-item']);
        $this->delete('auth_item_child', ['parent' => 'admin', 'child' => 'employee-rel-attachment']);
        $this->delete('auth_item_child', ['parent' => 'user', 'child' => 'employee-rel-attachment']);

        $this->delete('auth_item_child', ['parent' => 'moderator', 'child' => 'employee-rel-attachment/index']);
        $this->delete('auth_item_child', ['parent' => 'moderator', 'child' => 'employee-rel-attachment/update']);
        $this->delete('auth_item_child', ['parent' => 'moderator', 'child' => 'employee-rel-attachment/delete']);
        $this->delete('auth_item_child', ['parent' => 'moderator', 'child' => 'employee-rel-attachment/view']);
    }
}
