<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%file_attachment}}`.
 */
class m220630_051546_create_file_attachment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%file_attachment}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'native_path' => $this->string(500),
            'absolute_path' => $this->string(500),
            'hash_file' => $this->string(500),
            'type' => $this->string(100),
            'size' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%file_attachment}}');
    }
}
