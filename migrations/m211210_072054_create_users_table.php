<?php

use Cassandra\Date;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%hr_employees}}`
 */
class m211210_072054_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(),
            'password_hash' => $this->string(),
            'hr_employee_id' => $this->integer(),
            'password_reset_token' => $this->string(),
            'auth_key' => $this->string(),
            'status' => $this->smallInteger(),
            'created_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `hr_employee_id`
        $this->createIndex(
            '{{%idx-users-hr_employee_id}}',
            '{{%users}}',
            'hr_employee_id'
        );

        // add foreign key for table `{{%hr_employees}}`
        $this->addForeignKey(
            '{{%fk-users-hr_employee_id}}',
            '{{%users}}',
            'hr_employee_id',
            '{{%hr_employees}}',
            'id',
            'RESTRICT'
        );

        $this->insert('hr_employees', [
            'first_name' => 'Azizbek',
            'last_name' => 'Ismoilov',
            'father_name' => 'Abdullajon',
            'birth_date' => 850608000,
            'address' => 'Bogdod',
            'phone_number' => '+998-33-786-71-44',
            'status' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('hr_employees', [
            'first_name' => 'Ahadjon',
            'last_name' => 'Ismoilov',
            'father_name' => 'Abdullajon',
            'birth_date' => 753926400,
            'address' => 'Bogdod',
            'phone_number' => '+998-91-156-71-44',
            'status' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('hr_employees', [
            'first_name' => 'Avazbek',
            'last_name' => 'Ismoilov',
            'father_name' => 'Abdullajon',
            'birth_date' => 1008547200,
            'address' => 'Bogdod',
            'phone_number' => '+998-90-508-22-56',
            'status' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        $this->insert('users', [
            'username' => 'admin',
            'password_hash' => Yii::$app->security->generatePasswordHash(123456),
            'hr_employee_id' => 1,
            'auth_key' => 'qSn5DH7kIedrNo1YXIpZFNz-f67qWF8U',
			'password_reset_token' => null,
			'status' => 1,
			'created_at' => time(),
			'updated_at' => time(),
			'created_by' => 1,
			'updated_by' => 1,
        ]);
        $this->insert('users', [
            'username' => 'user',
            'password_hash' => Yii::$app->security->generatePasswordHash(123456),
            'hr_employee_id' => 2,
            'auth_key' => 'qSn5DH7kIedrNo1YXIpZFNz-f67qWF8U',
			'password_reset_token' => null,
			'status' => 1,
			'created_at' => time(),
			'updated_at' => time(),
			'created_by' => 1,
			'updated_by' => 1,
        ]);
        $this->insert('users', [
            'username' => 'moderator',
            'password_hash' => Yii::$app->security->generatePasswordHash(123456),
            'hr_employee_id' => 3,
            'auth_key' => 'qSn5DH7kIedrNo1YXIpZFNz-f67qWF8U',
			'password_reset_token' => null,
			'status' => 1,
			'created_at' => time(),
			'updated_at' => time(),
			'created_by' => 1,
			'updated_by' => 1,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%hr_employees}}`
        $this->dropForeignKey(
            '{{%fk-users-hr_employee_id}}',
            '{{%users}}'
        );

        // drops index for column `hr_employee_id`
        $this->dropIndex(
            '{{%idx-users-hr_employee_id}}',
            '{{%users}}'
        );

        $this->dropTable('{{%users}}');
    }
}

