<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employee_rel_attachment}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%hr_employees}}`
 * - `{{%file_attachment}}`
 */
class m220719_045058_create_employee_rel_attachment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employee_rel_attachment}}', [
            'id' => $this->primaryKey(),
            'hr_employee_id' => $this->integer(),
            'attachment_id' => $this->integer(),

            'type' => $this->smallInteger(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        // creates index for column `hr_employee_id`
        $this->createIndex(
            '{{%idx-employee_rel_attachment-hr_employee_id}}',
            '{{%employee_rel_attachment}}',
            'hr_employee_id'
        );

        // add foreign key for table `{{%hr_employees}}`
        $this->addForeignKey(
            '{{%fk-employee_rel_attachment-hr_employee_id}}',
            '{{%employee_rel_attachment}}',
            'hr_employee_id',
            '{{%hr_employees}}',
            'id',
            'RESTRICT'
        );

//        // creates index for column `attachment_id`
//        $this->createIndex(
//            '{{%idx-employee_rel_attachment-attachment_id}}',
//            '{{%employee_rel_attachment}}',
//            'attachment_id'
//        );
//
//        // add foreign key for table `{{%file_attachment}}`
//        $this->addForeignKey(
//            '{{%fk-employee_rel_attachment-attachment_id}}',
//            '{{%employee_rel_attachment}}',
//            'attachment_id',
//            '{{%file_attachment}}',
//            'id',
//            'RESTRICT'
//        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%hr_employees}}`
        $this->dropForeignKey(
            '{{%fk-employee_rel_attachment-hr_employee_id}}',
            '{{%employee_rel_attachment}}'
        );

        // drops index for column `hr_employee_id`
        $this->dropIndex(
            '{{%idx-employee_rel_attachment-hr_employee_id}}',
            '{{%employee_rel_attachment}}'
        );

        // drops foreign key for table `{{%file_attachment}}`
        $this->dropForeignKey(
            '{{%fk-employee_rel_attachment-attachment_id}}',
            '{{%employee_rel_attachment}}'
        );

        // drops index for column `attachment_id`
        $this->dropIndex(
            '{{%idx-employee_rel_attachment-attachment_id}}',
            '{{%employee_rel_attachment}}'
        );

        $this->dropTable('{{%employee_rel_attachment}}');
    }
}
