<?php

use yii\db\Migration;

/**
 * Class m220719_045051_insert_rbac_permissions
 */
class m220719_045051_insert_rbac_permissions extends Migration
{
    static $module = 'api';
    static $controller = 'auth-item';
    static $actions = ["index-rule","search","index-permission","view","create-rule","update-rule","create-permission","update-permission","delete"];
    static $roleName = 'role-auth-item';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->upsert('auth_item', ['name' => self::$roleName, 'type' => '1', 'description' => '','name_for_user' => 'Ruxsat va role permission']);
        foreach (self::$actions as $action) {
            $p =self::$controller . '/' . $action;
            $this->upsert('auth_item', ['name' => $p, 'type' => '2', 'description' => '']);
            $this->upsert('auth_item_child', ['parent' => self::$roleName, 'child' => $p]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        foreach (self::$actions as $action) {
            $p = self::$controller . '/' . $action;
            $this->delete('auth_item', ['name' => $p, 'type' => '2']);
            $this->delete('auth_item_child', ['parent' => self::$roleName, 'child' => $p]);
        }
        $this->delete('auth_item', ['name' => self::$roleName, 'type' => '1']);
    }
}
