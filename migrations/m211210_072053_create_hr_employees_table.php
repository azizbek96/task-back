<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%hr_employees}}`.
 */
class m211210_072053_create_hr_employees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%hr_employees}}', [
            'id' => $this->primaryKey(),
            
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'father_name' => $this->string(),
            'birth_date' => $this->integer(),
            'address' => $this->text(),
            'phone_number' => $this->string(50),

            'type' => $this->smallInteger(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%hr_employees}}');
    }
}
