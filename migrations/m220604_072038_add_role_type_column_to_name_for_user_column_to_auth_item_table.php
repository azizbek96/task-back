<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%auth_item}}`.
 */
class m220604_072038_add_role_type_column_to_name_for_user_column_to_auth_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%auth_item}}', 'role_type', $this->integer());
        $this->addColumn('{{%auth_item}}', 'name_for_user', $this->string());
        $this->addColumn('{{%auth_item}}', 'category', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%auth_item}}', 'role_type');
        $this->dropColumn('{{%auth_item}}', 'name_for_user');
        $this->dropColumn('{{%auth_item}}', 'category');
    }
}
